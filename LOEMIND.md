## Eesti keele lausestaja-sõnestaja konteiner

[ESTNLTK-l](https://github.com/estnltk/estnltk) põhinevat lausestajat-sõnestajat sisaldav tarkvara-konteiner (docker),
mille liides vastab [ELG nõuetele](https://european-language-grid.readthedocs.io/en/release1.0.0/all/LTInternalAPI.html#).
Teeb utf-8 vormingus tavateksti sobivaks
[morfoloogilist analüsaatorit sisaldavale konteinerile](https://gitlab.com/tarmo.vaino/docker-elg-morf).

## Mida sisaldab <a name="Mida_sisaldab"></a>

* [EstNLTK](https://github.com/estnltk/estnltk) lausestaja-sõnestaja <!--Seda ei paneks lingina GITLABi kuna tuleb paketihaldusest-->
* Konteineri ja liidesega seotud lähtekood

## Eeltingimused

* Peab olema paigaldatud tarkvara konteineri tegemiseks/kasutamiseks; juhised on [docker'i veebilehel](https://docs.docker.com/).
* Kui sooviks on lähtekoodi ise kompileerida või konteinerit kokku panna, siis peab olema paigaldatud versioonihaldustarkvara; juhised on [git'i veebilehel](https://git-scm.com/).

## Konteineri allalaadimine Docker Hub'ist

Valmis konteineri saab laadida alla Docker Hub'ist, kasutades Linux'i käsurida (Windows'i/Mac'i käsurida on analoogiline):

```commandline
docker pull tilluteenused/estnltk_tokenizer:1.0.0
```
 Seejärel saab jätkata osaga [Konteineri käivitamine](#Konteineri_käivitamine).

## Ise konteineri tegemine

### 1. Lähtekoodi allalaadimine 

```commandline
mkdir -p ~/git/ ; cd ~/git/
git clone https://gitlab.com/tilluteenused/docker_elg_estnltk_tokenizer.git docker_elg_estnltk_tokenizer_gitlab
```

### 2. Konteineri kokkupanemine

```commandline
cd ~/git/docker_elg_estnltk_tokenizer_gitlab
docker build -t tilluteenused/estnltk_tokenizer:1.0.0 .
```

## Konteineri käivitamine <a name="Konteineri_käivitamine"></a>

```commandline
docker run -p 6000:6000 tilluteenused/estnltk_tokenizer:1.0.0
```

Pole oluline, milline on jooksev kataloog terminaliaknas konteineri käivitamise hetkel.

Käivitatud konteineri töö lõpetab Ctrl+C selles terminaliaknas, kust konteiner käivitati.

## Päringu json-kuju

Tasub tähele panna, et Python'i json'i teek esitab teksti vaikimisi ASCII kooditabelis;
täpitähed jms esitatakse Unicode'i koodidena, nt. õ = \u00f5.

```json
{
  "type":"text",
  "content": string, /* "Analüüsimist vajav tekst" */
  "params":{...}     /* võib puududa */
}
```

## Vastuse json-kuju

```json
{
  "response": {
    "type": "annotations",
    "annotations": {
      "sentence": [        /* lausete massiiv */
        {
          "start": number,     /* lause algus tekstis (märgijadas) */
          "end": number        /* lause lõpp tekstis (märgijadas) */
        }
      ],
      "token": [           /* kõigi lausete kõigi sõnede massiiv */
        {
          "start": number, /* sõne algus tekstis (märgijadas) */
          "end": number,   /* sõne lõpp tekstis (märgijadas) */
          "features": {
            "token": string /* sõne */
          }
        }
      ]
    }
  }
}
```

## Kasutusnäide

```commandline
curl -i --request POST --header "Content-Type: application/json" --data '{"params":{"placeholder": "app specific flags"},"type":"text","content":"Mees peeti kinni. Sarved&Sõrad"}' http://localhost:6000/process
```

```json
HTTP/1.1 200 OK
Server: gunicorn
Date: Fri, 18 Feb 2022 13:08:06 GMT
Connection: close
Content-Type: application/json
Content-Length: 469

{"response":{"type":"annotations","annotations":{"sentence":[{"start":0,"end":17},{"start":18,"end":30}],"token":[{"start":0,"end":4,"features":{"token":"Mees"}},{"start":5,"end":10,"features":{"token":"peeti"}},{"start":11,"end":16,"features":{"token":"kinni"}},{"start":16,"end":17,"features":{"token":"."}},{"start":18,"end":24,"features":{"token":"Sarved"}},{"start":24,"end":25,"features":{"token":"&"}},{"start":25,"end":30,"features":{"token":"S\u00f5rad"}}]}}}
```

## Vaata lisaks

* [Eesti keele morfoloogilise analüsaatori konteiner](https://gitlab.com/tilluteenused/docker-elg-morf/)
* [Eesti keele morfoloogilise ühestaja konteiner](https://gitlab.com/tilluteenused/docker-elg-disamb/)

## Rahastus

Konteiner loodi EL projekti [Microservices at your service](https://www.lingsoft.fi/en/microservices-at-your-service-bridging-gap-between-nlp-research-and-industry) toel.

## Autorid

Konteineri autorid: Tarmo Vaino, Heiki-Jaan Kaalep

Konteineri sisu autoreid vt. jaotises [Mida sisaldab](#Mida_sisaldab) toodud viidetest.
 
