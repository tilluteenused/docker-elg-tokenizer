## Container with a tokenizer for Estonian

Container (docker) for tokenizer based on [ESTNLTK](https://github.com/estnltk/estnltk) with
interface compliant with [ELG requirements](https://european-language-grid.readthedocs.io/en/release1.0.0/all/LTInternalAPI.html#).

## Contains  <a name="Contains"></a>

* [EstNLTK](https://github.com/estnltk/estnltk) tokenizer 
* Container and interface code

## Preliminaries

* You should have software for making / using the container installed; see instructions on the [docker web site](https://docs.docker.com/).
* In case you want to compile the code or build the container yourself, you should have version control software installed; see instructions on the [git web site](https://git-scm.com/).

## Downloading image from Docker Hub

You may dowload a ready-made container from Docker Hub, using the Linux command line (Windows / Mac command lines are similar):

```commandline
docker pull tilluteenused/estnltk_tokenizer:1.0.0
```
Next, continue to the section [Starting the container](#Starting_the_container).

## Making your own container

### 1. Downloading the source code

```commandline
mkdir -p ~/git/ ; cd ~/git/
git clone https://gitlab.com/tilluteenused/docker_elg_estnltk_tokenizer.git docker_elg_estnltk_tokenizer_gitlab

### 2. Building the container

```commandline
cd ~/git/docker_elg_estnltk_tokenizer_gitlab
docker build -t tilluteenused/estnltk_tokenizer:1.0.0 .
```

## Starting the container <a name="Starting the container"></a>

```commandline
docker run -p 6000:6000 tilluteenused/estnltk_tokenizer:1.0.0
```

One need not be in a specific directory to start the container.

Ctrl+C in a terminal window with a running container in it will terminate the container.

## Query json

Note that the Python json library renders text in ASCII by default;
accented letters etc. are presented as Unicode codes, e.g. õ = \u00f5.

```json
{
  "type":"text",
  "content": string, /* "The text of the request" */
  "params":{...}     /* optional */
}
```

## Response json

```json
{
  "response": {
    "type": "annotations",
    "annotations": {
      "sentence": [        /* array of sentences */
        {
          "start": number,  /* beginning of sentence (offset in characters) */
          "end": number     /* end of sentence (offset in characters) */
        }
      ],
      "token": [           /* array of all the tokens of all the sentences */
        {
          "start": number, /* beginning of token (offset in characters) */
          "end": number,   /* end of token (offset in characters) */
          "features": {
            "token": string /* token */
          }
        }
      ]
    }
  }
}
```

## Usage example

```commandline
curl -i --request POST --header "Content-Type: application/json" --data '{"params":{"placeholder": "app specific flags"},"type":"text","content":"Mees peeti kinni. Sarved&Sõrad"}' http://localhost:6000/process
```

```json
HTTP/1.1 200 OK
Server: gunicorn
Date: Fri, 18 Feb 2022 13:08:06 GMT
Connection: close
Content-Type: application/json
Content-Length: 469

{"response":{"type":"annotations","annotations":{"sentence":[{"start":0,"end":17},{"start":18,"end":30}],"token":[{"start":0,"end":4,"features":{"token":"Mees"}},{"start":5,"end":10,"features":{"token":"peeti"}},{"start":11,"end":16,"features":{"token":"kinni"}},{"start":16,"end":17,"features":{"token":"."}},{"start":18,"end":24,"features":{"token":"Sarved"}},{"start":24,"end":25,"features":{"token":"&"}},{"start":25,"end":30,"features":{"token":"S\u00f5rad"}}]}}}
```

## See also

* [Container of a morphological analyzer for Estonian](https://gitlab.com/tilluteenused/docker-elg-morf)
* [Container of a morphological disambiguator](https://gitlab.com/tilluteenused/docker-elg-disamb/)

## Sponsors

The container development was sponsored by EU CEF project [Microservices at your service](https://www.lingsoft.fi/en/microservices-at-your-service-bridging-gap-between-nlp-research-and-industry)


## Authors

Authors of the container: Tarmo Vaino, Heiki-Jaan Kaalep

Authors of the contents of the container: see references at section [Contains](#Contains).
 
